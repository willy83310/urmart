from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0003_insertCustomerData'),
    ]

    def insertData(apps, schema_editor):
        Product = apps.get_model('orders', 'Product')
        Shop = apps.get_model('orders', 'Shop')
        product_data = [
            {
                "product_name": "香草風味杏仁奶",
                "stock_pcs": 500,
                "price": 249,
                "shop_id": Shop.objects.get(shop_id="um"),
                "vip": True,
            },
            {
                "product_name": "原味燕麥奶箱購6入組 ",
                "stock_pcs": 500,
                "price": 1254,
                "shop_id": Shop.objects.get(shop_id="um"),
                "vip": False,
            },
            {
                "product_name": "蛋白棒/堅果綜合嘗鮮組",
                "stock_pcs": 500,
                "price": 599,
                "shop_id": Shop.objects.get(shop_id="es"),
                "vip": True,
            },
            {
                "product_name": "乳清蛋白飲-琥珀歐蕾(黑糖牛奶)",
                "stock_pcs": 500,
                "price": 360,
                "shop_id": Shop.objects.get(shop_id="es"),
                "vip": False,
            },
            {
                "product_name": "蛋白威化餅乾-榛果巧克力",
                "stock_pcs": 500,
                "price": 999,
                "shop_id": Shop.objects.get(shop_id="tw"),
                "vip": True,
            },
        ]

        for ele in product_data:
            product = Product(
                product_name=ele["product_name"],
                stock_pcs=ele["stock_pcs"],
                price=ele["price"],
                shop_id=ele["shop_id"],
                vip=ele["vip"])
            product.save()

    operations = [
        migrations.RunPython(insertData),
    ]
