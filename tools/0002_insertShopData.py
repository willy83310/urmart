from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    def insertData(apps, schema_editor):
        Shop = apps.get_model('orders', 'Shop')

        shop_data = [
            {
                "shop_id": "um",
                "shop_name": "store1",
            },
            {
                "shop_id": "es",
                "shop_name": "store2",
            },
            {
                "shop_id": "tw",
                "shop_name": "store3",
            }
        ]

        for ele in shop_data:
            shop = Shop(
                shop_id=ele["shop_id"],
                shop_name=ele["shop_name"]
            )
            shop.save()

    operations = [
        migrations.RunPython(insertData),
    ]
