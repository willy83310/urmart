from urmart.settings.base import *

DEBUG = True
ALLOWED_HOSTS = ["*"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "urmart",  # here is import
        'USER': "root",
        'PASSWORD': "password",
        'HOST': "mysql",
        'PORT': 3306,
    },
}
