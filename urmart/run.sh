#!/usr/bin/env bash

# User Defined > OS Environment > Default Value
USER_DEFINED_SETTINGS_MODULE=${1:+urmart.settings.$1}
DJANGO_SETTINGS_MODULE=${USER_DEFINED_SETTINGS_MODULE:-$DJANGO_SETTINGS_MODULE}
export DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE:-urmart.settings.local}

echo ${DJANGO_SETTINGS_MODULE}

# export APP_VERSION=$(cat CHANGELOG.md | perl -ne 'print "$1\n" if /#* \[(\d+\.\d+\.\d+)\]/' | head -1)

python /src/urmart/manage.py migrate
python /src/urmart/manage.py runserver 0.0.0.0:8000