from django.http.response import JsonResponse
from django.shortcuts import render
from django.db import transaction, DatabaseError

# Create your views here.
from orders.models import Product, Order, Customer, Shop
from orders.serializers import ProductSerializer, OrderSerializer, CustomerSerializer, ShopSerializer

from rest_framework import viewsets
from rest_framework.decorators import action


def check_vip(func):
    def warp(request, *args, **kwargs):
        data = request.request.data
        if "customer_id" not in data.keys():
            return "no customer_id"
        customer_id = data.get("customer_id")
        product_id = data.get("product_id")
        customer = Customer.objects.get(customer_id=customer_id)
        vip = Product.objects.get(product_id=product_id).vip
        if customer.vip == False and vip == True:
            res = {
                "msg": "權限不足!!"
            }
            return JsonResponse(res)
        else:
            return func(request, *args, **kwargs)

    return warp


def check_stock(func):
    def warp(request, *args, **kwargs):
        if request.request.method == "DELETE":
            order_id = request.kwargs.get("pk")
            order = Order.objects.get(order_id=order_id)
            product = order.product_id
            if product.stock_pcs == 0:
                kwargs["arrival"] = True
            else:
                kwargs["arrival"] = False
            return func(request, *args, **kwargs)

        elif request.request.method == "POST":
            data = request.request.data
            product_id = data.get("product_id")
            qty = data.get("qty")
            product = Product.objects.get(product_id=product_id)
            if product.stock_pcs >= int(qty):
                return func(request, *args, **kwargs)
            else:
                res = {
                    "msg": "庫存不足!!"
                }
                return JsonResponse(res)

    return warp


# Create your views here.
class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    @check_vip
    @check_stock
    def create(self, request):
        data = request.data
        product_id = data.get("product_id")
        qty = data.get("qty")
        try:
            with transaction.atomic():
                product = Product.objects.get(product_id=product_id)
                product.stock_pcs = product.stock_pcs - int(qty)
                product.save()
                response = super(OrderViewSet, self).create(request)
                response = response.data
                response["msg"] = "訂單建立成功"
                # raise DatabaseError
        except DatabaseError:
            response = {
                "msg": "訂單建立失敗"
            }
        return JsonResponse(response)

    @check_stock
    def destroy(self, request, arrival, pk=None):
        order = Order.objects.get(order_id=pk)
        product = order.product_id
        response = {
            "msg": ""
        }
        try:
            with transaction.atomic():
                product.stock_pcs = product.stock_pcs + order.qty
                product.save()
                super(OrderViewSet, self).destroy(request)
                response["msg"] = "訂單刪除成功"
        except DatabaseError:
            response["msg"] = "訂單刪除失敗"

        return JsonResponse(response)

    def list(self, request):
        orders = []
        for order in Order.objects.select_related('product_id').all():
            product = order.product_id
            data = {
                "order_id": order.order_id,
                "product_id": order.product_id.product_id,
                "product_name": order.product_id.product_name,
                "qty": order.qty,
                "price": product.price,
                "shop_id": product.shop_id.shop_id,
                "customer_id": order.customer_id.customer_id,
            }
            orders.append(data)
        return JsonResponse(orders, safe=False)

    # 新增route完成自己想要回傳的data, detail=True:需接在pk之後,等於舊版的@detail_route , detail=False:直接接path,等於舊版的@list_route
    @action(detail=False, methods=['get'])
    def top3(self, request):
        results = Order.objects.raw(
            'select * from (SELECT *,sum(qty) as total_sales FROM urmart.`order` group by product_id order by total_sales desc limit 3 ) total left join urmart.`product` product on total.product_id=product.product_id ;')
        top3 = []
        for result in results:
            data = {
                "product_id": result.product_id.product_id,
                "product_name": result.product_id.product_name,
                "price": result.price,
                "stock_pcs": result.stock_pcs,
                "total_sales": int(result.total_sales),
                "shop_id": result.shop_id,
                "vip": result.vip,
            }
            top3.append(data)
        return JsonResponse(top3, safe=False)


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class ShopViewSet(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer
