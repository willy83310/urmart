from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0004_insertProductData'),
    ]

    def insertData(apps, schema_editor):
        Order = apps.get_model('orders', 'Order')
        Product = apps.get_model('orders', 'Product')
        Customer = apps.get_model('orders', 'Customer')

        order_data = [
            {
                "product_id": Product.objects.get(product_id=1),
                "qty": 100,
                "customer_id": Customer.objects.get(customer_id=1),
            },
            {
                "product_id": Product.objects.get(product_id=1),
                "qty": 23,
                "customer_id": Customer.objects.get(customer_id=1),
            },
            {
                "product_id": Product.objects.get(product_id=2),
                "qty": 231,
                "customer_id": Customer.objects.get(customer_id=2),
            },
            {
                "product_id": Product.objects.get(product_id=2),
                "qty": 12,
                "customer_id": Customer.objects.get(customer_id=2),
            },
            {
                "product_id": Product.objects.get(product_id=3),
                "qty": 315,
                "customer_id": Customer.objects.get(customer_id=1),
            },
            {
                "product_id": Product.objects.get(product_id=3),
                "qty": 41,
                "customer_id": Customer.objects.get(customer_id=1),
            },
            {
                "product_id": Product.objects.get(product_id=4),
                "qty": 164,
                "customer_id": Customer.objects.get(customer_id=2),
            },
            {
                "product_id": Product.objects.get(product_id=4),
                "qty": 32,
                "customer_id": Customer.objects.get(customer_id=2),
            },
            {
                "product_id": Product.objects.get(product_id=5),
                "qty": 200,
                "customer_id": Customer.objects.get(customer_id=1),
            },
            {
                "product_id": Product.objects.get(product_id=5),
                "qty": 16,
                "customer_id": Customer.objects.get(customer_id=1),
            },
            {
                "product_id": Product.objects.get(product_id=4),
                "qty": 60,
                "customer_id": Customer.objects.get(customer_id=1),
            },
        ]

        for ele in order_data:
            order = Order(
                product_id=ele["product_id"],
                qty=ele["qty"],
                customer_id=ele["customer_id"],
            )
            order.save()

    operations = [
        migrations.RunPython(insertData),
    ]
