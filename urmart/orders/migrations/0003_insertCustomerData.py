from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_insertShopData'),
    ]

    def insertData(apps, schema_editor):
        Customer = apps.get_model('orders', 'Customer')

        customer_data = [
            {
                "customer_name": "Willy",
                "vip": True,
            },
            {
                "customer_name": "Tom",
                "vip": False,
            }
        ]

        for ele in customer_data:
            customer = Customer(
                customer_name=ele["customer_name"],
                vip=ele["vip"]
            )
            customer.save()

    operations = [
        migrations.RunPython(insertData),
    ]
