from rest_framework import serializers
from orders.models import Product, Order, Customer, Shop


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        # fields = '__all__'
        fields = ('product_id', 'product_name',
                  'stock_pcs', 'price', 'shop_id', 'vip')


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        # fields = '__all__'
        fields = ('order_id', 'product_id', 'qty', 'customer_id')


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        # fields = '__all__'
        fields = ('customer_id', 'customer_name', 'vip')


class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        # fields = '__all__'
        fields = ('shop_id', 'shop_name')
