from django.db import models

# Create your models here.


class Product(models.Model):
    product_id = models.AutoField(primary_key=True)
    product_name = models.CharField(
        max_length=20,
        db_column="product_name",
        primary_key=False,
        unique=False,
    )
    stock_pcs = models.PositiveIntegerField(
        db_column="stock_pcs",
        primary_key=False,
        unique=False,
    )
    price = models.PositiveIntegerField(
        db_column="price",
        primary_key=False,
        unique=False,
    )
    shop_id = models.ForeignKey(
        db_column="shop_id",
        to="Shop",
        to_field="shop_id",
        on_delete=models.CASCADE,
    )
    vip = models.BooleanField(
        db_column="vip",
        primary_key=False,
        unique=False,
    )
    last_modify_date = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "product"


class Order(models.Model):
    order_id = models.AutoField(primary_key=True, )
    product_id = models.ForeignKey(
        db_column="product_id",
        to="Product",
        to_field="product_id",
        on_delete=models.CASCADE,
    )
    qty = models.PositiveIntegerField(
        db_column="qty",
        primary_key=False,
        unique=False,
    )
    customer_id = models.ForeignKey(
        db_column="customer_id",
        to="Customer",
        to_field="customer_id",
        on_delete=models.CASCADE
    )
    last_modify_date = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "order"


class Customer(models.Model):
    customer_id = models.AutoField(primary_key=True, )
    customer_name = models.CharField(
        max_length=20,
        db_column="customer_name",
        primary_key=False,
        unique=False,
    )
    vip = models.BooleanField(
        db_column="vip",
        primary_key=False,
        unique=False,
    )
    last_modify_date = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "customer"


class Shop(models.Model):
    shop_id = models.CharField(
        max_length=5,
        db_column="shop_id",
        primary_key=True,
        unique=True,
    )
    shop_name = models.CharField(
        max_length=20,
        db_column="shop_name",
        primary_key=False,
        unique=False,
    )
    last_modify_date = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "shop"
