# build frontend code

FROM node:latest as build
WORKDIR /src
COPY . .
RUN cd frontend && yarn install && yarn build


FROM python:3.7.1

WORKDIR /src
COPY --from=build /src .
RUN pip install -r requirements.txt
EXPOSE 8000

CMD ["sh","/src/urmart/run.sh","prod"]

