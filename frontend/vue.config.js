const path = require('path');

function resolve(dir) {
  return path.join(__dirname, '.', dir);
}

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/static/dist/' : 'http://127.0.0.1:8081',
  outputDir: '../urmart/static/dist',

  // relative to outputDir!
  indexPath: '../../templates/base-vue.html',

  configureWebpack: {
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.esm.js',
        '@': resolve('src'),
        $scss: resolve('src/assets/scss'),
        $images: resolve('src/assets/images'),
      },
      extensions: ['*', '.js', '.vue', '.json'],
    },
  },

  chainWebpack: (config) => {
    /*
      The arrow function in writeToDisk(...) tells the dev server to write
      only index.html to the disk.
      The indexPath option (see above) instructs Webpack to also rename
      index.html to base-vue.html and save it to Django templates folder.
      We don't need other assets on the disk (CSS, JS...) - the dev server
      can serve them from memory.
      See also:
      https://cli.vuejs.org/config/#indexpath
      https://webpack.js.org/configuration/dev-server/#devserverwritetodisk-
      */
    config.devServer
      .public('http://127.0.0.1:8081')
      .hotOnly(true)
      .headers({ 'Access-Control-Allow-Origin': '*' })
      .writeToDisk((filePath) => filePath.endsWith('index.html'));
  },

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [resolve('src/assets/scss/myStyle.scss')],
    },
  },
};
