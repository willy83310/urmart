import axios from 'axios';

// Product相關的api
const productRequest = axios.create({
    baseURL: `${process.env.VUE_APP_API_SERVER}/api/product/`
});

// Order相關的api
const orderRequest = axios.create({
    baseURL: `${process.env.VUE_APP_API_SERVER}/api/order/`
});


// Product相關的api
export const apiProductList = () => productRequest.get('');
export const apiProduct = data => productRequest.get(`${data.product_id}/`);

// Order相關的api
export const apiOrderList = () => orderRequest.get('');
export const apiOrder = order_id => orderRequest.get(`${order_id}/`);
export const apiOrderCreate = data => orderRequest.post('', data);
export const apiOrderDelete = order_id => orderRequest.delete(`${order_id}/`);
export const apiOrderTop3 = () => orderRequest.get('top3/');

