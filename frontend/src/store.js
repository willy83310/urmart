/* eslint-disable no-underscore-dangle */
import Vue from 'vue';
import Vuex from 'vuex';
import { apiProductList, apiOrderList, apiOrderTop3 } from './api'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        productList: [],
        orderList: [],
        top3: [],
        alertMsg: ""
    },
    mutations: {
        setProductList(state, productList) {
            state.productList = productList;
        },
        setOrderList(state, orderList) {
            state.orderList = orderList;
        },
        setAlertMsg(state, msg) {
            state.alertMsg = msg;
        },
        setTop3(state, top3) {
            state.top3 = top3;
        }
    },
    actions: {
        async getProductData({ commit }) {
            try {
                const response = await apiProductList();
                commit('setProductList', response.data)
            } catch (err) {
                console.error(err);
            }
        },
        async getOrderData({ commit }) {
            try {
                const response = await apiOrderList();
                commit('setOrderList', response.data)
            } catch (err) {
                console.error(err);
            }
        },
        async getTop3Data({ commit }) {
            try {
                const response = await apiOrderTop3();
                var el = document.querySelector("div.top3-table");
                el.setAttribute("style", "display:inline");
                commit('setTop3', response.data)
            } catch (err) {
                console.error(err);
            }
        },
    },
});

export default store;
